import {LitElement,html} from 'lit-element';

class PersonaStats extends LitElement {
//quitamos el render pq no vamos a pintar nada con el en la pagina

    static get properties() {
        return{
            people: {type: Array}
        };
    }
    
    constructor() {
        super();

        this.people = [];
    }

    updated(changedProperties) {
        console.log("updated");

        // viene de persona APP con el comando de la linea 59 de persona APP
        //             this.shadowRoot.querySelector("persona-stats").people = this.people;

        if (changedProperties.has("people")) {
            console.log("ha cambiado el valor de la propiedad people en persona stats")

            let peopleStats = this.gatherPeoplArrayInfo(this.people);

            this.dispatchEvent(
                new CustomEvent("updated-people-stats", {
                
                    detail: {
                        peopleStats: peopleStats
                    }
                }
                )
            );
        }
    }

    gatherPeoplArrayInfo(people) {
        console.log("gatherPeoplArrayInfo");

        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        return peopleStats;
    }

}

customElements.define('persona-stats',PersonaStats)