import{LitElement, html } from 'lit-element' ;
class FichaPersona extends LitElement {   
  static get properties() {
		return {
			name: {type: String},			
      yearsInCompany: {type: Number},			
      personInfo: {type: String},
			photo: {type: Object}			
		};
  }	
  
  constructor() {
      super();

      this.name = "Prueba Nombre";
      this.yearsInCompany ="12";
      this.updatePersonInfo();
      this.photo = {
        src: "./img/persona.jpg",
        alt: "foto persona"			
      };		


  }

  render() { 
		return html`
     <div>
         <label for="fname">Nombre Completo</label>
         <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
         <br />						
         <label for="yearsInCompany">Años en la empresa</label>
         <input type="text" name="yearsInCompany" value="${this.yearsInCompany}"  @input="${this.updateYearsInCompany}"></input>
         <br />			
         <input type="text" value="${this.personInfo}" disabled></input>
         <br />
         <!--<img src="./img/persona.jpg" height="200" width="200" alt="Foto persona"> -->

     </div>
		`;
  }

 updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue)


      if (changedProperties.has("yearsInCompany")) {
        console.log("Propiedad yearsInCompany cambiada valor anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
        this.updatePersonInfo();
      }

    });

 }
 
updateName(e){
  console.log("updateName");
  this.name = e.target.value;
}

updateYearsInCompany(e){
  console.log("updateYears");
  this.yearsInCompany = e.target.value;
  console.log("updateYears" + this.yearsInCompany );
}

updatePersonInfo(){
  console.log("updatePersonInfo");
  console.log("yearsInCompany vale " + this.yearsInCompany);

  if (this.yearsInCompany >= 7){
    this.personInfo = "lead";
  } else if (this.yearsInCompany >= 5){
    this.personInfo = "senior";
  }else if (this.yearsInCompany >= 3){
    this.personInfo ="team";
  } else {
    this.personInfo = "junior";
  }
 }
}  

customElements.define('ficha-persona', FichaPersona)