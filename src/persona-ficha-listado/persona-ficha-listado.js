import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties() {
        return {
            fname: {type: String},
            yearsInCompany: {type: Number},
            profile: {type: String},
            photo: {type: Object}
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
           <div class="card h-100">
             <img src="${this.photo.src}" alt="${this.photo.alt}" height="300" width="250" class="card-img-top"/>
                <div class="card-body">
                    <h5 class="card-title">${this.fname}</h5>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
                    </ul>
                </div> 
                <div class="card-footer">
                <button @click="${this.deletePerson}" class="btn btn-danger col-5"> <strong> X </strong> </button>
                <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"> <strong> Info </strong> </button>
                </div>
            </div>               
            </div>            
        `;


    }

    deletePerson(e){

        console.log("deletePerson en persona-ficha-listado");
        console.log(" se va a borrar  " + this.fname);
//Enviará el nombre a Persona-Main
        this.dispatchEvent( 
            new CustomEvent("delete-person", {
                    "detail": {
                        "name": this.fname
                    }
                }
            )
        
        );
    }

    moreInfo(e) {

        console.log("more info")
        console.log("se ha pedido mas informacion de la persona " + this.fname);
//Pide más información.
        this.dispatchEvent(
                new CustomEvent("info-person", {
                    detail: {
                        name: this.fname
                    }

                }
            )
        );
    }
}
customElements.define('persona-ficha-listado', PersonaFichaListado)