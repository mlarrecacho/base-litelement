import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm:{type: Boolean}
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name: "Manuel",
                yearsInCompany: 10,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Manuel"
                }, 
                profile: "Empleado 1"
            }, {
                name: "Manuel 2",
                yearsInCompany: 2,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Manuel 2"
                }, 
                profile: "Empleado 2."
            }, {
                name: "Éowyn 1",
                yearsInCompany: 5,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Éowyn"
                }, 
                profile: "Empleado 3."
            },
            {
                name: "Éowyn 2",
                yearsInCompany: 5,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Éowyn"
                }, 
                profile: "Empleado 4."
            },
            {
                name: "Éowyn 3",
                yearsInCompany: 5,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Éowyn"
                }, 
                profile: "Empleado 5."
            }
        ];
        this.showPersonForm = false;
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
                
          <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`<persona-ficha-listado 
                                        fname="${person.name}" 
                                        yearsInCompany="${person.yearsInCompany}"
                                        profile="${person.profile}"
                                        .photo="${person.photo}"
                                        @delete-person="${this.deletePerson}"
                                        @info-person="${this.infoPerson}"
                                    >
                                </persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                    <persona-form 
                     @persona-form-close="${this.personFormClose}"
                     @persona-form-store="${this.personFormStore}"
                    class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
        `;
    }
//función que nos da Litelement

    updated(changedProperties) {
        console.log("update");

        if(changedProperties.has("showPersonForm")) {

            console.log("ha cambiado el valor de la propiedad show personaform en persona-main");

            if (this.showPersonForm === true) {
                this.showPersonFormData();
            }    else {
                    this.showPersonList();
            }
        } 
// si ha cambiado el array se cambia en el modo de dart de alta una persona para que se detecte el cambio
        if (changedProperties.has("people")){
            console.log("ha cambiado el valor de la propiedad people en  persona-main");

            //lanzamos el evento con el array hacia persona APP
            this.dispatchEvent(
                new CustomEvent("updated-people", {
                    detail: { people: this.people }
                }
                )
            );
        }
    }

    showPersonList(){
        console.log("showPersonList")
        console.log("mostrando listado de personas")
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData(){
        console.log("showPersonFormData")
        console.log("mostrar el formulario de persona")
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    }






    personFormClose(e) {
        console.log("personFormClose  " ); 
        console.log("Se ha cerrado el formulario de persona"); 

        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore  " ); 
        console.log("se va a almacenar una persona"); 
        console.log("la propiedad name vale " + e.detail.person.name);
        console.log("la propiedad profile vale " + e.detail.person.profile);
        console.log("la propiedad years in company  vale " + e.detail.person.yearsInCompany);
        console.log("la propiedad edición tiene el valor " + e.detail.editingPerson);

        console.log(e.detail.person); 

        if (e.detail.editingPerson == true) {
            console.log("Persona a actualizar" + e.detail.person.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name     
                //hacemos un ternario si es verdadero ejecuta lo de la izq si no lo de la derecha
                   ? person = e.detail.person : person
            );

//            let indexOfPerson =  this.people.findIndex(
//                     person => person.name === e.detail.person.name
//            );
// ponemos el array original igual a lo que nos han modificado
//            if (indexOfPerson >= 0 ){
//                 console.log("persona encontrada");
//                     this.people[indexOfPerson] = e.detail.person;
//            }
            
            
        } else {
            this.people.push(e.detail.person);
            console.log("Persona Nueva Almacenada"); 

        }
        

        this.showPersonForm = false;
        

    }




    deletePerson(e){

        console.log("deletePerson en persona-main"); 
        console.log("me piden borrar " + e.detail.name); 
        console.log("personas antes de borrar  " ); 
        console.log(this.people); 

        //la función filter es una función del array, como map
        // => es el cuerpo de la función...
        this.people =this.people.filter(
             person => person.name != e.detail.name     
        );

        //forzamos la actualizacion y que vuelva a pasar por el render. 
        // this.requestUpdate();

        console.log("personas despues de borrar  " ); 
        console.log(this.people); 

    }
    infoPerson(e){

        console.log("infoPerson en persona-main"); 
        console.log("me piden información " + e.detail.name); 
    
        // filter devuelve un array 
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        console.log(chosenPerson);
        
        let personToShow = {};
        personToShow.name = chosenPerson[0].name;
        personToShow.profile = chosenPerson[0].profile;
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany;

      
        this.shadowRoot.getElementById("personForm").person = personToShow;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
      // activimaos el formulario paraq ue se muestree.  
        this.showPersonForm = true;
    

    }

    
}

customElements.define('persona-main', PersonaMain)