import { LitElement, html, css } from 'lit-element';


class TestBootStrap extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    static get styles() {
        return css`
        .redbg {
            backgroun-color: red;
        }
        .greenbg {
            background-color: green;
        }
        .bluebg {
            background-color: blue;
        }
        .greybg{
            background-color: grey;
        }
        `}

    render() {
        return html`
             <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
             <h3>Test BootStrat</h3>
             <div class="row greybg">
                <div class="col-2 offset-1">Col 1</div>
                <div class="col-3 col-sm-1 bluebg">Col 2</div>
                <div class="col-4 col-sm 1 greenbg">Col 3</div>
             </div>
        `;
    }
}

customElements.define('test-bootstrap', TestBootStrap)