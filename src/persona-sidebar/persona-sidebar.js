import {LitElement,html} from 'lit-element';

class PersonaSidebar extends LitElement {


    static get properties() {
        return {
            peopleStats: {type: Object}
        }
    }


    constructor() {
        super();

        this.peopleStats = {};
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
          
        <div>Persona SideBar</div>
<aside>
    <section>
        <div>
            hay <span> ${this.peopleStats.numberOfPeople}</span> personas
        </div>
        <div class="mt-5">
            <button @click="${this.newPerson}" class="w-100 btn btn-success" style="font-size: 50px"> <strong>+</strong> </button>
        </div>
    </section>
</aside>

        `;


    }

    newPerson(e){

        console.log("newPerson en persona-sidebar");
        console.log("se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent("new-person", {}));

    }

}

customElements.define('persona-sidebar',PersonaSidebar)